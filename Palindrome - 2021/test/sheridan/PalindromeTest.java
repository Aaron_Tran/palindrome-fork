package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindrome( ) {
		assertTrue( "Invalid Palindrome", Palindrome.isPalindrome("racecar") );
	}

	@Test
	public void testIsPalindromeNegative( ) {
		assertFalse( "Invalid Palindrome", Palindrome.isPalindrome("cat"));
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		assertTrue( "Invalid Palindrome", Palindrome.isPalindrome("a") );
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		assertFalse( "Invalid Palindrome", Palindrome.isPalindrome("edit on tide"));
	}	
	
}
